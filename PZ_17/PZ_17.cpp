﻿#include <iostream>
#include <cmath>
using namespace std;

class Vector {

private:
    double x;
    double y;
    double z;

public:
    Vector() : x(0), y(0), z(0)
    {}

    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void Show()
    {
        cout << '\n' << x << ' ' << y << ' ' << z << '\n';
       
    }
    
    double length() {
        return sqrt(x * x + y * y + z * z);
      
    }
    


};


int main()
{
    setlocale(0, "");

    Vector V;
    V.Show();
    Vector V2(10, 20, 3);
    V2.Show();
    V2.length();
    cout << "Длина вектора 1 = " << V.length() << endl;
    cout << "Длина вектора 2 = " << V2.length() << endl;

}

