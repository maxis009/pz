﻿#include <iostream>

using namespace std;

class Animal
{
 public:
     virtual void Voice() const = 0;
};

class Cat : public Animal
{
public:
    void Voice() const override
    {
        cout << "Meow\n";
    }

};

class Dog : public Animal
{
public:
    void Voice() const override
    {
        cout << "Gav\n";
    }
};

class Pig : public Animal
{
public:
    void Voice() const override
    {
        cout << "Hry\n";
    }

};



int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Pig();

    for (Animal* a : animals)
        a->Voice();;
}


